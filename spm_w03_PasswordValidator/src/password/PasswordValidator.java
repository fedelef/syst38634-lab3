package password;

/*
 * @author Fernando Fedele 991529962
 * 
 * Class to validate whether a password meets the specified requirements:
 *     - password needs to be 8 chars or larger
 *     - password contains at least 2 digits
 *     
 * Assumptions:
 *     - spaces are not valid characters for the purpose of calculating length
 */

public class PasswordValidator {
	
	private static final int MIN_LENGTH = 8;
	private static final int MIN_DIGITS = 2;

	public static boolean isValidLength(String password) {
		if (password.isEmpty()) {
			throw new IllegalArgumentException("password cannot be empty");
		} 
		
		return password.length() >= MIN_LENGTH;
	}
	
	
	public static boolean hasValidDigits(String password) {
		if (password.isEmpty()) {
			throw new IllegalArgumentException("password cannot be empty");
		} 
		
		int digitCount = 0;
		for (char c : password.toCharArray()) {
			if (Character.isDigit(c)) {
				digitCount++;
			}
		}
		
		return digitCount >= MIN_DIGITS;
	}
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*");
	}
}




























