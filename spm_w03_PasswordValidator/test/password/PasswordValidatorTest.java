package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Fernando Fedele 991529962
 * 
 * Testing class to ensure the PasswordValidator works as intended
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("AbCdEfGh"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOut1() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOut2() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	
	//////////////////////////////////////////////////////////////////////////

	@Test
	public void testIsValidLengthRegular() {
		String password = "123456789101112131415";
		assertTrue("password is incorrect length", PasswordValidator.isValidLength(password));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIsValidLengthException() {
		PasswordValidator.isValidLength("");
		fail("should have thrown exception");
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("password is incorrect length", result);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("password is incorrect length", result);
	}
	
	
	//////////////////////////////////////////////////////////////////////////
	
	@Test
	public void testIsValidDigitsRegular() {
		boolean result = PasswordValidator.hasValidDigits("12345678910");
		assertTrue("invalid number of digits", result);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testIsValidDigitsException() {
		PasswordValidator.hasValidDigits("");
		fail("invalid number of digits");
	}
	
	@Test
	public void testIsValidDigitsBoundaryIn() {
		boolean result = PasswordValidator.hasValidDigits("ab1cde2fg");
		assertTrue("invalid number of digits", result);
	}
	
	@Test
	public void testIsValidDigitsBoundaryOut() {
		boolean result = PasswordValidator.hasValidDigits("ab0cdefg");
		assertFalse("invalid number of digits", result);
	}
}
